import pytest
from xlm import license

"""
total_sampleN have the total number of licenses that
are installed, and have the checked out licenses by user@machine

available_sampleN have the current available licenses (total - checked out)
"""


@pytest.fixture
def total_sample0():
    with open("tests/sample/rlm_all0.txt") as file:
        return file.read()


@pytest.fixture
def total_sample0_value():
    value = [('mpp-multihost-so', 'v20220108', '2'), ('mpp-multihost-so', 'v20220108', '1')]
    return value


@pytest.fixture
def total_sample1():
    with open("tests/sample/rlm_all1.txt") as file:
        return file.read()


@pytest.fixture
def total_sample1_value():
    value = [('nuke_i', 'v2019.0107', '2'), ('nuke_i', 'v2020.0107', '1'), ('nukex_i', 'v2020.0107', '1'), ('nuke_i', 'v2022.0107', '19'), ('nuke_r', 'v2022.0107', '70'), ('nukexassist_i', 'v2022.0107', '24'), ('nukex_i', 'v2022.0107', '11'), ('ocula_nuke_r', 'v4.0', '18'), ('ocula_nuke_i', 'v4.0', '1'), ('foundry_production_i', 'v2022.0107', '0'), ('hieroplayer_i', 'v2022.0107', '21')]
    return value


@pytest.fixture
def total_sample2():
    with open("tests/sample/rlm_all2.txt") as file:
        return file.read()


@pytest.fixture
def total_sample2_value():
    value = [('neatvideo-ofx', 'v5.999', '15'), ('neatvideo-ofx', 'v5.999', '5')]
    return value


@pytest.fixture
def available_sample0_value():
    value = [('mpp-multihost-so', 'v20220108', '2'), ('mpp-multihost-so', 'v20220108', '1')]
    return value


@pytest.fixture
def available_sample0():
    with open("tests/sample/rlm_avail0.txt") as file:
        return file.read()


@pytest.fixture
def available_sample1_value():
    value = [('nuke_i', 'v2019.0107', '2'), ('nuke_i', 'v2020.0107', '1'), ('nukex_i', 'v2020.0107', '1'), ('nuke_i', 'v2022.0107', '9'), ('nuke_r', 'v2022.0107', '70'), ('nukexassist_i', 'v2022.0107', '24'), ('nukex_i', 'v2022.0107', '9'), ('ocula_nuke_r', 'v4.0', '18'), ('ocula_nuke_i', 'v4.0', '1'), ('foundry_production_i', 'v2022.0107', '1'), ('hieroplayer_i', 'v2022.0107', '21')]
    return value


@pytest.fixture
def available_sample1():
    with open("tests/sample/rlm_avail1.txt") as file:
        return file.read()


@pytest.fixture
def available_sample2_value():
    value = [('neatvideo-ofx', 'v5.999', '15'), ('neatvideo-ofx', 'v5.999', '5')]
    return value


@pytest.fixture
def available_sample2():
    with open("tests/sample/rlm_avail2.txt") as file:
        return file.read()


def test_find_licenses_with_no_server_output():
    """
    test find_licenses without server_output
    """
    with pytest.raises(TypeError):
        license.extract_licenses()


def test_find_licenses_with_empty_server_output():
    """
    test find_licenses with an empty server response
    """
    assert len(license.extract_licenses("", key='available')) == 0
    assert len(license.extract_licenses("", key='total')) == 0


def test_find_licenses_sample0_available(available_sample0,
                                         available_sample0_value):
    received_value = license.extract_licenses(available_sample0, key='available')
    assert received_value == available_sample0_value


def test_find_licenses_sample1_available(available_sample1,
                                         available_sample1_value):
    received_value = license.extract_licenses(available_sample1, key='available')
    assert received_value == available_sample1_value


def test_find_licenses_sample2_available(available_sample2,
                                         available_sample2_value):
    received_value = license.extract_licenses(available_sample2, key='available')
    assert received_value == available_sample2_value


def test_find_licenses_sample0_total(total_sample0, total_sample0_value):
    received_value = license.extract_licenses(total_sample0, key='total')
    assert received_value == total_sample0_value


def test_find_licenses_sample1_total(total_sample1, total_sample1_value):
    received_value = license.extract_licenses(total_sample1, key='total')
    assert received_value == total_sample1_value


def test_find_licenses_sample2_total(total_sample2, total_sample2_value):
    received_value = license.extract_licenses(total_sample2, key='total')
    assert received_value == total_sample2_value


def test_get_license_usage_without_input():
    """
    test get_license_usage with no input
    """
    with pytest.raises(TypeError):
        license.get_license_users()


def test_get_license_usage_with_empty_input():
    """
    test get_license_usage with empty string
    """
    assert license.get_license_users('') == []

def test_get_license_usage_with_total_sample0(total_sample0):
    """
    no license are checked out in sample0
    should return an empty list
    """
    received_value = license.get_license_users(total_sample0)
    assert len(received_value) == 0


def test_get_license_usage_with_total_sample1(total_sample1):
    """
    many licenses are checked out in sample1
    """
    received_value = license.get_license_users(total_sample1)
    assert len(received_value) == 12


def test_get_license_usage_with_total_sample2(total_sample2):
    """
    no licenses are checkout in sample2
    """
    received_value = license.get_license_users(total_sample2)
    assert len(received_value) == 0


def test_get_license_usage_with_user_filter(total_sample1):
    user = 'user4'
    received_value = license.get_license_users(total_sample1, user=user)
    assert len(received_value) == 1


def test_get_license_usage_with_non_existant_user_filter(total_sample1):
    user = 'notvaliduser'
    received_value = license.get_license_users(total_sample1, user=user)
    assert len(received_value) == 0


def test_get_license_usage_with_product_filter(total_sample1):
    product = 'nuke'
    received_value = license.get_license_users(total_sample1, product=product)
    assert len(received_value) == 12


def test_get_license_usage_with_non_existant_product_filter(total_sample1):
    product = 'nonvalid product'
    received_value = license.get_license_users(total_sample1, product=product)
    assert len(received_value) == 0


def test_get_license_usage_with_user_and_product_filter(total_sample1):
    user = 'user7'
    product = 'nuke'
    received_value = license.get_license_users(total_sample1, user=user,
                                               product=product)
    assert len(received_value) == 1


def test_get_license_usage_with_non_existant_user_and_product_filter(total_sample1):
    user = 'nonexistantuser'
    product = 'nuke'
    received_value = license.get_license_users(total_sample1, user=user,
                                               product=product)
    assert len(received_value) == 0


def test_get_license_usage_with_user_and_non_existant_product(total_sample1):
    user = 'user1'
    product = 'nonexistantproduct'
    received_value = license.get_license_users(total_sample1, user=user,
                                               product=product)
    assert len(received_value) == 0


def test_combine_products_count_without_arguments():
    with pytest.raises(TypeError):
        license.merge_products()


def test_combine_products_count_with_available_sample1(available_sample1_value):
    key = 'available'
    received_value = license.merge_products(available_sample1_value,
                                            key=key)
    assert len(received_value[key]) == 8


def test_combine_products_count_with_total_sample1(total_sample1_value):
    key = 'total'
    received_value = license.merge_products(total_sample1_value,
                                            key=key)
    assert len(received_value[key]) == 8


def test_combine_products_count_with_available_sample1_versions(available_sample1_value):
    key = 'available'
    versions = True
    received_value = license.merge_products(available_sample1_value,
                                            key=key,
                                            keep_versions=versions)
    assert len(received_value[key]) == 11


def test_combine_products_count_with_total_sample1_versions(total_sample1_value):
    key = 'total'
    versions = True
    received_value = license.merge_products(total_sample1_value,
                                            key=key,
                                            keep_versions=versions)
    assert len(received_value[key]) == 11


def test_combine_products_count_with_non_iterable_product():
    key = 'total'
    received_value = license.merge_products(123213, key=key)
    assert received_value == {key:{}}

