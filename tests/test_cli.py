import pytest
from xlm import license, cli

rlm_server = '4101@127.0.0.1'


@pytest.fixture
def avail_products_without_versions():
    with open("tests/sample/rlm_avail1.txt") as file:
        output = file.read()
    key = 'available'
    raw_products = license.extract_licenses(output, key)
    products = license.merge_products(raw_products, key)
    return products


@pytest.fixture
def avail_products_with_versions():
    with open("tests/sample/rlm_avail1.txt") as file:
        output = file.read()
    key = 'available'
    raw_products = license.extract_licenses(output, key)
    products = license.merge_products(raw_products, key,
                                      keep_versions=True)
    return products


@pytest.fixture
def total_products_without_versions():
    with open("tests/sample/rlm_all1.txt") as file:
        output = file.read()
    key = 'total'
    raw_products = license.extract_licenses(output, key)
    products = license.merge_products(raw_products, key)
    return products


@pytest.fixture
def total_products_with_versions():
    with open("tests/sample/rlm_all1.txt") as file:
        output = file.read()
    key = 'total'
    raw_products = license.extract_licenses(output, key)
    products = license.merge_products(raw_products, key,
                                      keep_versions=True)
    return products


@pytest.fixture
def license_users():
    with open('tests/sample/rlm_all1.txt', 'r') as file:
        output = file.read()
    users = license.get_license_users(output)
    return users


def test_pretty_print_licenses_avail(avail_products_without_versions, capsys):
    key = 'available'
    cli.pretty_print_licenses(avail_products_without_versions, key, rlm_server)
    captured = capsys.readouterr()
    assert len(captured.out.strip().split('\n')) == 9


def test_pretty_print_licenses_avail_with_versions(avail_products_with_versions, capsys):
    key = 'available'
    cli.pretty_print_licenses(avail_products_with_versions, key, rlm_server)
    captured = capsys.readouterr()
    assert len(captured.out.strip().split('\n')) == 12


def test_pretty_print_licenses_total(total_products_without_versions, capsys):
    key = 'total'
    cli.pretty_print_licenses(total_products_without_versions, key, rlm_server)
    captured = capsys.readouterr()
    assert len(captured.out.strip().split('\n')) == 9


def test_pretty_print_licenses_total_with_versions(total_products_with_versions, capsys):
    key = 'total'
    cli.pretty_print_licenses(total_products_with_versions, key, rlm_server)
    captured = capsys.readouterr()
    assert len(captured.out.strip().split('\n')) == 12


def test_pretty_print_users(license_users, capsys):
    cli.pretty_print_users(license_users, rlm_server)
    captured = capsys.readouterr()
    assert len(captured.out.strip().split('\n')) == 13


def test_pretty_print_users_with_no_users(capsys):
    cli.pretty_print_users([], rlm_server)
    captured = capsys.readouterr()
    assert len(captured.out.strip().split('\n')) == 1

