import pytest
from rlm import server
import subprocess

rlm_server = "4101@127.0.0.1"


def test_get_licenses_with_available_key(mocker):
    """
    test get_licenses with 'available' as key
    """
    command = f"rlmutil rlmstat -c {rlm_server} -avail"
    command = command.split(" ")
    mocker.patch("subprocess.Popen")
    assert server.query_server(rlm_server, "available")
    subprocess.Popen.assert_called_with(command, stdout=subprocess.PIPE)


def test_get_licenses_with_total_key(mocker):
    """
    test get_licenses with 'total' as key
    """
    command = f"rlmutil rlmstat -c {rlm_server} -a"
    command = command.split(" ")
    mocker.patch("subprocess.Popen")
    assert server.query_server(rlm_server, "total")
    subprocess.Popen.assert_called_with(command,
                                        stdout=subprocess.PIPE)


def test_get_licenses_without_key(mocker):
    """
    test get_licenses without key
    """
    command = f"rlmutil rlmstat -c {rlm_server}".split(' ')
    mocker.patch("subprocess.Popen")
    with pytest.raises(TypeError):
        server.query_server(rlm_server)


def test_get_licenses_without_server(mocker):
    """
    test get_licenses without server
    """
    with pytest.raises(TypeError):
        server.query_server(key='total')


def test_get_liceses_without_argument():
    """
    test get_licenses without any argument
    """
    with pytest.raises(TypeError):
        server.query_server()


def test_get_licenses_oserror(mocker):
    command = f"rlmutil rlmstat -c {rlm_server}"
    mocker.patch("subprocess.Popen", side_effect=OSError("no such file"))

    with pytest.raises(SystemExit):
        server.query_server(rlm_server, "total")


def test_get_licenses_calls_rlmutil_with_total_key_and_product(mocker):
    command = f'rlmutil rlmstat -c {rlm_server} -a -p nuke_i'
    command = command.split(' ')
    mocker.patch('subprocess.Popen')
    assert server.query_server(rlm_server, 'total', 'nuke_i')
    subprocess.Popen.assert_called_with(command, stdout=subprocess.PIPE)


def test_get_licenses_calls_rlmutil_with_available_key_and_product(mocker):
    command = f'rlmutil rlmstat -c {rlm_server} -avail -p nuke_i'
    command = command.split(' ')
    mocker.patch('subprocess.Popen')
    assert server.query_server(rlm_server, 'available', 'nuke_i')
    subprocess.Popen.assert_called_with(command, stdout=subprocess.PIPE)

