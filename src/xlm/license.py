import re


def extract_licenses(server_query_output: str, key: str) -> list:
    """
    key: available
    parses the output of `lmutil lmstat -c <server> -avail [-p <>]`
    return a list of tuples containing the regex capture groups
    format: [("product", "version", "count")]

    key: total
    parses the output of `lmutil lmstat -c <server> -a [-p <>]`
    return a list of tuples containing the regex capture groups
    format: [("product","version","count")]
    """
    license_filter = ''
    if key == 'available':
        license_filter = '[^\\n](.*)\\ (v.*)\\ available: (\\d{1,})'
    elif key == 'total':
        license_filter = '[^\\n](.*)\\ (v.*),\\ pool:\\ \\d{1,4}\\s+count: (\\d{1,})'

    try:
        licenses = re.findall(license_filter, server_query_output)
    except TypeError as err:
        print(f'Error: {list(err.args)[0]}')
        return []

    return licenses


def get_license_users(server_query_output, product='', user='') -> list:
    """
    parses the output of `lmutil lmstat -c <server> -a`
    Optional: option to filter by product, user
    return a list of tuples containing the regex capture groups
    format: [("product","version","user")]
    """
    license_filter = f'[^\\n]({product}.*?) (.*?): ({user}.*?) .*\\(handle: '

    try:
        matches = re.findall(license_filter, server_query_output)
        matches = list(dict.fromkeys(matches).keys())
        return matches
    except TypeError as err:
        print(f'Error: {list(err.args)[0]}')
        return []


def merge_products(products: list, key: str, keep_versions=False) -> dict:
    """
    combine available license count by product
    Optional: keep versions of product
    products is a list of tuples
    format: [("product","version", "count")]
    """
    products_combined = {key: {}}

    # check if products is iterable before for loop
    # if not, return empty dict with key
    try:
        _ = iter(products)
    except TypeError as err:
        print(f'Error: {list(err.args)[0]}')
        return products_combined

    for product in products:
        name, version, count = product

        if keep_versions is True:
            name = f'{name}@{version}'

        if name not in products_combined[key]:
            products_combined[key].update({name: int(count)})
        else:
            products_combined[key][name] += int(count)

    return products_combined

