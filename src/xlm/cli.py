import argparse
import dotenv
import sys
import os
import xlm


def create_parser() -> argparse.ArgumentParser:
    """
    create parser for cli
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-s',
                        default='',
                        metavar='port@ip',
                        help='licence server to connect')
    parser.add_argument('--servers',
                        action='store_true',
                        help='Requires RLM_SERVERS environment variable to be set')
    parser.add_argument('--versions',
                        action='store_true',
                        help='include product versions in output')
    parser.add_argument('-k',
                        default='available',
                        choices=['available', 'total'],
                        help='which licenses to get')
    parser.add_argument('--users',
                        action='store_true',
                        help='show whois using the licenses')

    return parser


def create_args(parser: argparse.ArgumentParser) -> argparse.Namespace:
    """
    validates certain parameter
    """
    args = parser.parse_args()

    # exit if RLM_SERVERS is not set
    if args.servers:
        dotenv.load_dotenv()
        rlm_servers = os.getenv("RLM_SERVERS") or ''
        if rlm_servers == '':
            print(f'Error: RLM_SERVERS environment variable is not set')
            sys.exit(1)

    return args


def pretty_print_licenses(products: dict, key: str, rlm_server: str):
    """
    show the licenses in a pretty format
    """
    print(f'\n{key.title()} licenses on {rlm_server}:')
    products = list(products[key].items())

    # sort by product name length
    products.sort(key=len)
    for product in products:
        name, count = product
        print(f'\t{name}: {count}')


def pretty_print_users(users: list, rlm_server: str):
    """
    show who's using the licences in a pretty format
    users: list is in format [("product","version","user@machine")]
    """
    if len(users) == 0:
        print(f'\nThere are currently no users on {rlm_server}')
    else:
        print(f'\nUsers on {rlm_server}:')
        for entry in users:
            product, version, user = entry
            print(f'\t{product}@{version} : {user}')


def main():
    import rlm
    args = create_args(create_parser())
    rlm_servers = []

    # add the rlm servers pass by environment variable
    # or by cli to the list
    if args.servers:
        for rlm_server in os.getenv('RLM_SERVERS').split(' '):
            rlm_servers.append(rlm_server)
    if args.s:
        rlm_servers.append(args.s)

    for rlm_server in rlm_servers:
        output = rlm.server.query_server(rlm_server, args.k)
        raw_products = xlm.license.get_license_users(output)
        products = xlm.license.merge_products(raw_products, args.k, keep_versions=args.versions)
        pretty_print_licenses(products, args.k, rlm_server)

        if args.users:
            # need to make a new server query that has
            # the users information if key was 'available'
            if args.k == 'available':
                output = rlm.server.query_server(rlm_server, 'total')
            users = xlm.license.get_licence_usage(output)
            pretty_print_users(users, rlm_server)
