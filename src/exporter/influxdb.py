from influxdb_client import InfluxDBClient, Point
from influxdb_client.client.write_api import SYNCHRONOUS
from datetime import datetime


def create_write_client(server_url: str, token: str, org: str) -> InfluxDBClient:
    client = InfluxDBClient(
        url=server_url,
        token=token,
        org=org
    )
    client = client.write_api(write_options=SYNCHRONOUS)
    return client


def send_record(client: InfluxDBClient, bucket: str, record: dict):
    key = list(record.keys())[0]

    for item in list(record[key].items()):
        product, count = item
        point = Point("licenses")
        point.tag("product", product)
        point.field(key, count)
        client.write(bucket=bucket, record=point)

