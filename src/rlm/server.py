import subprocess


def query_server(server: str, key: str, product='') -> bytearray:
    """
    query the license server to obtain the total of licenses count.
    (total per product/version)
    """
    product_filter = ''
    if product:
        product_filter = f' -p {product}'

    if key == 'total':
        command = f'rlmutil rlmstat -c {server} -a{product_filter}'
    elif key == 'available':
        command = f'rlmutil rlmstat -c {server} -avail{product_filter}'

    try:
        content = subprocess.Popen(command.split(' '), stdout=subprocess.PIPE)
    except OSError as err:
        print(f"Error: {err.strerror}")
        exit(err.errno)

    return content.communicate()[0].decode('UTF-8')
