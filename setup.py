from setuptools import setup, find_packages

with open("README.md", "r", encoding="UTF-8") as file:
    readme = file.read()

setup(
    name="python-xlm",
    version="0.1.0",
    description="Module to interface with rlm or flexlm license server",
    long_description=readme,
    author="novakele",
    author_email="python-xlm@correo.breakme.ca",
    install_requires=["influxdb-client", "python-dotenv"],
    packages=find_packages("src"),
    package_dir={"":"src"},
    entry_points={
        "console_scripts" : [
            "python-xlm=xlm.cli:main",
        ]
    }
)
