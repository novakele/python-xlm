.PHONY: test install

default: test

test:
	PYTHONPATH="./src" pytest -vv

install:
	pipenv install --dev --skip-lock
