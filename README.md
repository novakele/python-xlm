# python-xlm
This project is currently under development.
Expect the main branch to be unstable for now.

## Objective
The goal of this project is to provide a centralized tool to obtain information from license servers.  
The information gathered is either formatted for a human or a monitoring solution.  

The modules are meant to provide flexibility for system administrators and developers  
to write their own integration.

## Description
This is a wrapper around `rlmutil` and `lmutil`.

For now, it has basic functionalities to query one or more RLM servers to get the
available licenses and its users.

## Structure
The project si composed of a few modules:
- exporter -> format the metrics in a format that can be ingested by multiple tools (zabbix, influxdb, etc.)
- flexlm -> handles flexlm output format
- rlm -> handles rlm output format
- xlm -> handles common operation that are common to modules above

## Upcoming
- support for flexlm servers
- influxdb support



## Installation
### Requirements
- python >= 3.6
- `rlmutil` and `lmutil` in your PATH
### Instructions
1. Clone repository: `git clone git@gitlab.com:novakele/python-xlm`
2. `cd` into the repository.
3. `python setup.py install`

## Usage
- run `python-xlm`
- use wrapper `PYTHONPATH=./src python main.py`

```
usage: python-xlm [-h] [-s port@ip] [--servers] [--versions] [-k {available,total}] [--users]

optional arguments:
  -h, --help            show this help message and exit
  -s port@ip            licence server to connect
  --servers             Requires RLM_SERVERS environment variable to be set
  --versions            include product versions in output
  -k {available,total}  which licenses to get
  --users               show whois using the licenses
```


## Development
### Getting started

1. Ensure `pip` and `pipenv` are installed
2. Clone repository: `git clone git@gitlab.com:novakele/python-xlm`
3. `cd` into the repository.
4. Fetch development dependencies `make install`
5. Activate virtualenv: `pipenv shell`

### Running tests

Run tests locally using `make` if virtualenv is active:

```
$ make
```
if virtualenv isn't active then use:

```
$ pipenv run make
```

